﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Networking;
public class PongScore : NetworkBehaviour {

   
    public Text Canvas_Text;
    [SyncVar(hook = "OnScoreChange")]
    public int Leftscore;
    [SyncVar(hook = "OnScoreChange2")]
    public int Rightscore; 

    void Start()
    {
        Leftscore = 0;
        Rightscore = 0;
        Canvas_Text.text = Leftscore + "/" + Rightscore;
    }
    

    public void Add_Score(bool LR, int score_mod)
    {
        if (LR)
        {
            Leftscore += score_mod;
        }
        else
        {
            Rightscore += score_mod;
        }
        Canvas_Text.text = Leftscore + "/" + Rightscore;
    }
   
    private void OnScoreChange(int newValue)
    {
        Leftscore = newValue;
        Canvas_Text.text = Leftscore + "/" + Rightscore;
    }
    private void OnScoreChange2(int newValue)
    {
        Rightscore = newValue;
        Canvas_Text.text = Leftscore + "/" + Rightscore;
    }

   /* void OnStartClient()
    {
        Leftscore = 0;
        Rightscore = 0;

        OnScoreChange(Leftscore);
        OnScoreChange2(Rightscore);// Call it directly to update UI Text component on other clients who join the game later.
    }*/

}

