﻿using UnityEngine;
using UnityEngine.Networking;

public class PongBall : NetworkBehaviour
{


    public float Ball_speed = 5.0f;
    public GameObject pongscore;
    private Vector3 bstart;
    private Vector3 velo;

    void Start()
    {
        bstart = transform.position;
        Vector3 v = new Vector3(-1, 0, Random.Range(-1f, 1.0f));
        GetComponent<Rigidbody>().AddForce( v * Ball_speed,ForceMode.VelocityChange);
        
    }
    void restart(bool LR)

    {
        transform.position = bstart;
        pongscore.GetComponent<PongScore>().Add_Score(LR, 1);     
    }
  

void OnCollisionEnter(Collision col)
    {  
        switch (col.transform.tag)
        {
            case "OOBL":
                restart(true);
                break;
            case "OOBR":
                restart(false);
                break;
            case "Paddle":
                Vector3 local_vec = transform.InverseTransformPoint((col.contacts[0].point));
                hit(local_vec.z);
                break;
        }
    }
    void hit(float localz)
    {
        Debug.Log("hit");
        //pongscore.GetComponent<PongScore>().Add_Score(false, 1);
    }

}
