﻿using UnityEngine;
using System.Collections;

public class ThemeManager : MonoBehaviour
{

    // Asset Themes
    public bool customtheme = false;
    public int currenttheme = 0;
    public string[] themenames;
    private string PathName;
    public MeshRenderer[] assets; //background , ball, center line, paddle, wall **effect
    void Start()
    {
#if UNITY_ANDROID
                       PathName= Application.dataPath;
#else
        PathName = "File://" + Application.dataPath;
#endif
      if (customtheme)
        {
            Processtheme(currenttheme);
        }
      else
        {
            StartCoroutine(Checktheme());
        }
    }
    public void Processtheme(int theme)
    {
        //3 themes currently availble , normally use themes.length
        if (theme < 0 || theme > 2)//fallback
        {
            theme = 0;
        }

        StartCoroutine(Processtheme_change(theme));


    }
    IEnumerator Processtheme_change(int theme)
    {

        WWW www = new WWW(PathName + "/Themes/" + themenames[theme] + "/background0.png");
        yield return www;
        Texture2D tex = new Texture2D(www.texture.width, www.texture.height);
        tex = www.texture;
        assets[0].material.mainTexture = tex;
        www = new WWW(PathName + "/Themes/" + themenames[theme] + "/ball0.png");
        yield return www;
        Texture2D tex2 = new Texture2D(www.texture.width, www.texture.height);
        tex2 = www.texture;
        assets[1].sharedMaterial.mainTexture = tex2;
        www = new WWW(PathName + "/Themes/" + themenames[theme] + "/center_line0.png");
        yield return www;
        Texture2D tex3 = new Texture2D(www.texture.width, www.texture.height);
        tex3 = www.texture;
        assets[2].material.mainTexture = tex3;
        www = new WWW(PathName + "/Themes/" + themenames[theme] + "/paddle0.png");
        yield return www;
        Texture2D tex4 = new Texture2D(www.texture.width, www.texture.height);
        tex4 = www.texture;
        assets[3].sharedMaterial.mainTexture = tex4;
        www = new WWW(PathName + "/Themes/" + themenames[theme] + "/wall.png");
        yield return www;
        Texture2D tex5 = new Texture2D(www.texture.width, www.texture.height);
        tex5 = www.texture;
        assets[4].material.mainTexture = tex5;
        Debug.Log("theme updated: " + theme);
    }
    IEnumerator Checktheme()
    {
        WWW www = new WWW(PathName + "/Themes/CurrentTheme.txt");//local path used, replace this with remote server
         yield return www;
        if (www.text != "")
        {
            currenttheme = int.Parse(www.text);
            Debug.Log("Theme: " + currenttheme);
        }
               
    }
    
}
