﻿using UnityEngine;
using UnityEngine.Networking;

public class PaddleControl : NetworkBehaviour {
    public float speed_factor = 1.0f;
    public string axis = "Vertical";
   
    void Update()
    {
        if (!isLocalPlayer)
        {
            // exit from update if this is not the local player
            return;
        }

        float v = Input.GetAxisRaw(axis);
        GetComponent<Rigidbody>().velocity = new Vector3(0,0, v * speed_factor);

    }
}
