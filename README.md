# README #

Pong MP with asset control.

### What is this repository for? ###

* Simple pong clone with additional features.
* 0.01 - Empty project with README.md
* 0.02 - Single player pong game uploaded
* 0.03 - 2 player local added (p1 w,s p2 up down)
* 0.04 - 2 player networking added (issues with score remain)
* 0.05 - 2 player score fix (syncvar used)
* 0.06 - asset control using themes (theme manager)

### Who do I talk to? ###

* Sonny J Gray - Commadorecoder64@gmail.com.
